//
//  CAFViewController.h
//  Calculator
//
//  Created by Christopher Falck on 3/20/14.
//  Copyright (c) 2014 FalckTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAFViewController : UIViewController

@property double    holdFinalScreenNumber;//contatenates left and right Decimal
@property double    holdFirstNumScreen; //holds display value
@property double    holdSecondNumScreen; //holds display value
@property BOOL      clearOnNextPress; // use after equals and typing again, init NO

@property double    leftOfDecimal;//holds whole values
@property double    rightOfDecimal;//holds decimal values
@property int       decimalSelector;//init value = 1 (not in decimal at start)
@property int      afterDecimalPosition;//decides whether or not the values edits right or left of decimal;
                            //init values = NO
@property int       screenSelector; //if 1, update firstNumScreen otherwise secondNumScreen

-(void)     masterController: (double) numPassed;//performs work of passed button value
-(void)     updateScreen: (double) updateScreenWith;
-(double)   updateDecimalPosition: (double) digitToUpdate;//divides by ten to obtain the correct button value decimal position
-(void) resetDefaultValues;

@end


