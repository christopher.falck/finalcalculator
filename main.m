//
//  main.m
//  Calculator
//
//  Created by Christopher Falck on 3/20/14.
//  Copyright (c) 2014 FalckTech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CAFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CAFAppDelegate class]));
    }
}
